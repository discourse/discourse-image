# Discourse Image

This project will host the Discourse images for CERN.

## Composition

This repository is composed of the following files.

### Dockerfile

`Dockerfile` file is based on the following <https://github.com/discourse/discourse_docker/blob/main/image/base/slim.Dockerfile> file, with some extra modifications, such as the version of the PostgreSQL, and some extra permissions to make it work under OpenShift.

### Templates

Templates folder contains files (templates) that are based on the ones from <https://github.com/discourse/discourse_docker/blob/main/templates/web.template.yml> and <https://github.com/discourse/discourse_docker/blob/main/templates/redis.template.yml>, again with some modifications to make them work under OpenShift. Note that some modifications made at `templates\web.template.yml` are targeted on files located under the <https://github.com/discourse/discourse/tree/tests-passed> repository, such as the <https://github.com/discourse/discourse/blob/tests-passed/config/unicorn.conf.rb> file, or the <https://github.com/discourse/discourse/blob/tests-passed/config/nginx.sample.conf> file, among others.

## Tracking changes down upstream

In order to update the Discourse image, it is worth noting that our deployment is based on the upstream `tests-passed` branch, as stated in `params.version` under the `templates\web.template.yml` file. There are several articles upstream encouraging users to use the `tests-passed` branch, as explained in <https://meta.discourse.org/t/which-branch-of-discourse-is-used-internally/75181>.

Basically, we need to look for changes (commits) performed on both repositories, [discourse_docker](https://github.com/discourse/discourse_docker) and [discourse - branch tests-passed](https://github.com/discourse/discourse/tree/tests-passed), and see whether they need to be included into our image or not, or they need some adaptation to our deployment. It is very uncommon that we need to perform any modification/change, but if it happens, we need to know where to perform it.

Note that to build our image, we are using a tool called [`pups`](https://github.com/discourse/pups), widely used by the Discourse Team and included in the base image, which allows us to modify files without creating them from scratch (or to extract them entirely from the repository). These modifications can be done _on-the-fly_ (see `templates\web.template.yml` for examples).

- **Base Image**: Every now and then, the Discourse Team updates the version of base image that we use to build our image. The name of the base image is given under the <https://github.com/discourse/discourse_docker/blob/main/launcher> file, concretely under the `image="discourse/base:2.0.xxxxxxxx-xxxx"` value. Our `Dockerfile` must be updated with the `discourse/base:2.0.xxxxxxxx-xxxx` value.

- **PostgreSQL version**: Each base image (`discourse/base:2.0.xxxxxxxx-xxxx`) comes with a version of PostgreSQL installed (PG version that comes by default is easily identifiable by running `docker run -it --rm discourse/base:2.0.xxxxxxxx-xxxx` and then run `cat /etc/postgresql/<pg_version>/` within the container). Sometimes, to use a different version is needed other than the one that comes by default. For that, consider having a look to the desired version of PostgreSQL that we want to deploy at the appropriate postgresql template under <https://github.com/discourse/discourse_docker/tree/master/templates> (and then select any of the `postgres.<version>.template.yml` templates). For the record, it is common to include a command such `DEBIAN_FRONTEND=noninteractive apt-get purge -y postgresql-15 postgresql-client-15 postgresql-contrib-15` in the Dockerfile to remove the undesired version of PostgreSQL.

- **Nginx configuration** files [tests-passed/config/nginx.sample.conf](https://github.com/discourse/discourse/blob/tests-passed/config/nginx.sample.conf) and `/etc/nginx/nginx.conf` (provided with the Nginx installation in the base image): The `nginx.sample.conf` file will end up under the `/etc/nginx/conf.d/discourse.conf` location in our final image (see `templates\web.template.yml`), and no modification uses to be needed. For the `/etc/nginx/nginx.conf` file, it needs some adaptations to make it work under OpenShift, as stated in `templates\web.template.yml`.

- **Unicorn configuration** file [tests-passed/config/unicorn.conf.rb](https://github.com/discourse/discourse/blob/tests-passed/config/unicorn.conf.rb): This file will end up under the `/var/www/discourse/config/unicorn.conf.rb` location in our final image. This file requires special attention since it is the key for our instance to work. For the time being, we just redirect unicorn logs to console.

- **Discourse configuration** file [tests-passed/config/discourse_defaults.conf](https://github.com/discourse/discourse/blob/tests-passed/config/discourse_defaults.conf): This file will end up under the `/var/www/discourse/config/discourse.conf` location in our final image. The `/etc/runit/1.d/copy-env` file in `templates\web.template.yml` takes all `DISCOURSE_` environment variables, and populates them all into the `/var/www/discourse/config/discourse.conf` file. Whether we need to update any of these variables, we perform the appropiate changes under <https://gitlab.cern.ch/discourse/discourse-operator/-/blob/master/config/crd/bases/discourse.webservices.cern.ch_discourses.yaml> and <https://gitlab.cern.ch/discourse/discourse-operator/-/blob/master/helm-charts/discourse/values.yaml>. Thus, the CR will take them into account for any modification done under our Discourse instance.

- **Precompiling assets**: Precompiling assets is very common in the Ruby on Rails world. This precompilation is done in build time under the upstream Discourse repository. But in our case, as of version `v3.3.0.beta2`, this is done partially in build time (CI) and partially in runtime (init container). Discourse has enabled two ways of precompiling assets, one is via `rake assets:precompile:build` and `rake assets:precompile`. Special attention must be put to the `SKIP_EMBER_CLI_COMPILE` env variable, check under <https://github.com/discourse/discourse/blob/tests-passed/lib/tasks/assets.rake>:

  - `rake assets:precompile:build` (and `SKIP_EMBER_CLI_COMPILE=0`): is meant to be consumed in build time, and does not need any DB nor redis connection to be executed. This can be safely executed in build time (CI).
  - `rake assets:precompile` (and `SKIP_EMBER_CLI_COMPILE=1`): on the other hand, the precompile action does need a DB and a redis connection to succeed (see reasonale under <https://meta.discourse.org/t/rake-assets-precompile-without-database/126779>), therefore we will execute it in runtime (as part of the init container).

  The reason behind splitting this rake tasks, we assume is the result of a terrible memory consumption when running both rake tasks in runtime, that has impacted several customers (see for example <https://meta.discourse.org/t/rebuild-failed-look-for-help/306089>).
  In our case, and to be on the safe side, apart from splitting these two rake tasks between build and run time, there will be also a series of limitations introduced to contain a bit the memory consumption and the parallelism. Those are:

  - The use of `NODE_OPTIONS='--max-old-space-size=2048'`, to contain the memory heap.
  - The use of `JOBS=1`, to disable parallelism in ember (or more concretely, in the broccoli-babel, see <https://github.com/babel/broccoli-babel-transpiler?tab=readme-ov-file#number-of-jobs>).
  
  Background: as of `v2.9.0.beta1` version, Discourse is pushing their deployment to generate assets from the `ember-cli` tool. This requires substantial amount of memory to perform such precompilation, hence we will do it as part of the init container provided in our image. Note also that, as a consequence of using ember, ember expects having assigned an uid for the user who is running the application. In the case of OpenShift, this random uid is not added to the `/etc/passwd` automatically, hence we need to do it before performing any action to guarantee a successful precompliation. See <https://gitlab.cern.ch/discourse/discourse-image/-/issues/1> for a more detailed explanation of this critical component. This precompilation is executed as part of a rake task under <https://github.com/discourse/discourse/blob/tests-passed/lib/tasks/assets.rake>.

- **Redis configuration** file: Everything related to Redis is stated under the `templates\redis.template.yml`, which follows the <https://github.com/discourse/discourse_docker/blob/main/templates/redis.template.yml> upstream template. It is uncommon to have to make any modification on the Redis configuration.

## Update procedure

### Discourse image upgrade

After verification of potential changes under the above files, it is time to generate a new image. We normally create a new image every time a new release is launched by the Discourse Team (see <https://meta.discourse.org/tag/release-notes>). Since we follow the `tests-passed` branch approach, we need to look for the `v3.X.Y.betaZ-dev` releases (for example, `v3.1.0.beta5-dev`).

Clone this repository and create a new branch with the same name as the new release:

```bash
git clone https://gitlab.cern.ch/discourse/discourse-image.git && cd discourse-image

# For example, git checkout -b v3-1-0-beta2-dev
git checkout -b v3-X-Y-betaZ-dev

#
# Do modifications if needed
#

# Push changes
git add --all
git commit -m 'Bump version to v3.X.Y.betaZ-dev'
git push origin v3-X-Y-betaZ-dev
```

This will create a _ready-to-go_ image with the `v3-X-Y-betaZ-dev` tag under the <https://registry.cern.ch/> > project `Discourse` repository. For developing/testing purposes, consider updating now the <https://gitlab.cern.ch/discourse/discourse-operator/-/blob/master/helm-charts/discourse/values.yaml> file, concretely the `containers.webapp.image` value, and follow the instructions under <https://gitlab.cern.ch/discourse/discourse-operator> to deploy this new image.

Once we have ready our new image, Open a MR and merge it to master. The CI/CD process will automatically generate a production tag, with the following pattern `registry.cern.ch/discourse/discourse-image:RELEASE.YYYY.MM.DDTHH-mm-ssZ`. It is then time to update the <https://gitlab.cern.ch/discourse/discourse-operator/-/blob/master/helm-charts/discourse/values.yaml> file again, follow the <https://gitlab.cern.ch/discourse/discourse-operator> instructions to create a new version of the Discourse Operator and deploy it to the clusters.

!!! info
    Note that in order to access the <https://registry.cern.ch/> > project `Discourse` repository, user must belong to the `openshift-admins` e-group.

### Redis image upgrade

It's very uncommon that we have to upgrade Redis to a newer version. Note that by doing so, we will lose all sidekiq jobs in the queue of every instance, so sites may present a momentaneous hiccup whenever the new version of redis is deployed. This has no terrible consequences, but we could potentially lose any email notification enqueued that never will reach the target user, just to name a few.

For this specific image, we will use a dedicate branch name (instead of the `registry.cern.ch/discourse/discourse-image:RELEASE.YYYY.MM.DDTHH-mm-ssZ` pattern). We will use a `registry.cern.ch/discourse/discourse-image:redis-X-Y-Z` pattern to identify that indeed, this image is specifically used for the Redis image. This is to avoid any accidental deletion of the images, having in mind the space we were assigned under Harbor (as of 12th Jan 2022, we have 25GB).

To achieve this, just do the following. To get the Redis version, just go to any of the redis pod and execute `redis-cli --version`:

```bash
git clone https://gitlab.cern.ch/discourse/discourse-image.git && cd discourse-image

# For example, git checkout -b redis-6-2-6
git checkout -b redis-X-Y-Z

# Push changes
git add --all
git commit -m 'Generate a static version of Redis-X-Y-Z'
git push origin redis-X-Y-Z
```

The generated image will be used under <https://gitlab.cern.ch/discourse/discourse-operator/-/blob/master/helm-charts/discourse/values.yaml>, concretely under the `redis.image` element.

N.B.: The resulting `registry.cern.ch/discourse/discourse-image:RELEASE.YYYY.MM.DDTHH-mm-ssZ` pattern when merging can be securely deleted, to avoid confusion.

## Misc

For further information about Discourse in general, refer to the upstream forum <https://meta.discourse.org>.
