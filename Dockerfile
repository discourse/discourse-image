# Check https://github.com/discourse/discourse_docker/blob/master/launcher
FROM discourse/base:2.0.20250129-0720

ENV \
    # DBOD PostgreSQL version
    PG_MAJOR=13 \
    RUBY_ALLOCATOR=/usr/lib/libjemalloc.so \
    HOME=/var/www/discourse/ \
    RAILS_ENV=production \
    RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:${PATH} \
    DESCRIPTION="Platform for running Discourse instances" \
    PATH=/pups/bin/:${PATH} \
    LEFTHOOK=0

LABEL maintainer="Discourse Administrators <discourse-admins@cern.ch>" \
      io.openshift.expose-services="8080,6379:http" \
      description="$DESCRIPTION" \
      io.k8s.description="$DESCRIPTION" \
      name="Discourse for CERN"

COPY templates/ .

RUN \
    ## Install PG_13 for CERN
    ## ffi.:
    ## - https://github.com/discourse/discourse_docker/blob/master/templates/postgres.13.template.yml
    ## - FEATURE: Update base image and set default to postgresql 15:
    ##  - https://github.com/discourse/discourse_docker/commit/d9c837c783b06ae33d949f9dc50db187ba3d6337
    ##
    ## As of base image version 2.0.20250129-0720, PG_15 comes installed by default. Remove it to make PG_13 instead.
    DEBIAN_FRONTEND=noninteractive apt-get purge -y postgresql-15 postgresql-client-15 postgresql-contrib-15 postgresql-15-pgvector && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    apt -y update && \
    apt -y install postgresql-${PG_MAJOR} postgresql-client-${PG_MAJOR} \
    postgresql-contrib-${PG_MAJOR} postgresql-${PG_MAJOR}-pgvector && \
    apt clean && \
    ## Discourse specific bits
    ## Adding safe directory to avoid "fatal: detected dubious ownership in repository at '/var/www/discourse' and '/var/www/discourse/plugins/*'".
    find /var/www/discourse/ -name '.git' -type d -exec bash -c 'git config --global --add safe.directory ${0%/.git}' {} \; && \
    ## Drop the root user and change file and directory permissions.
    pups web.template.yml && \
    pups redis.template.yml && \
    CHECK_DIRS="/var/www/discourse/ \
                /etc/nginx/ \
                /var/lib/nginx/ \
                /etc/redis/ \
                /usr/local/etc/ \
                /etc/runit/ \
                /etc/service/" && \                
    mkdir -p $CHECK_DIRS && \
    chgrp -R 0 $CHECK_DIRS && \
    chmod -R g=u $CHECK_DIRS && \
    # Adjust permissions on /etc/passwd so writable by group root
    # see https://gitlab.cern.ch/discourse/discourse-operator/-/issues/1
    chmod g+w /etc/passwd

WORKDIR /var/www/discourse/

### entrypoint will be overriden for init-containers, nginx and redis
ENTRYPOINT ["/etc/service/unicorn/run"]